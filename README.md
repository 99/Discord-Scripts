## Discord Scripts
A collection of console scripts for [discord](https://discord.com/)

## Discord System
Gives you the verified system tag

```js
var findModule = (item) => Object.values(webpackJsonp.push([[],{['']:(_,e,r)=>{e.cache=r.c}}, [['']]]).cache).find(m=>m.exports&&m.exports.default&&m.exports.default[item]!==void 0).exports.default;
findModule('getCurrentUser').getCurrentUser().system = true;
```
Credit: [Daddy Fweak](https://gitdab.com/fweak1337)

## Change system tag
Changes the system tag

```js
var findModule = (item) => Object.values(webpackJsonp.push([[],{['']:(_,e,r)=>{e.cache=r.c}}, [['']]]).cache).find(m=>m.exports&&m.exports.default&&m.exports.default[item]!==void 0).exports.default;

findModule('Messages').Messages.SYSTEM_DM_TAG_SYSTEM = 'System Tag Name Here';
```
Credit: [Daddy Fweak](https://gitdab.com/fweak1337)


## Get all Badges
Gives you most of the discord badges

```js
Object.values(webpackJsonp.push([[],{[''] :(_,e,r)=>{e.cache=r.c}},
[['']]]).cache).find(m=>m.exports&&m.exports.default&&m.exports.default.getCurrentUser!==void
0).exports.default.getCurrentUser().flags=-33
```

Credit: [Daddy Zebratic](https://github.com/Zebratic)

## Get Bot Tag
Gives you bot tag

```js
var findModule = (item) => Object.values(webpackJsonp.push([[],{['']:(_,e,r)=>{e.cache=r.c}}, [['']]]).cache).find(m=>m.exports&&m.exports.default&&m.exports.default[item]!==void 0).exports.default;
findModule('getCurrentUser').getCurrentUser().bot = true;
```

## Get Token
Gives you the token of your account

```js
Object.values(webpackJsonp.push([[],{['']:(_,e,r)=>{e.cache=r.c}},[['']]]).cache).find(m=>m.exports&&m.exports.default&&m.exports.default.getToken!==void 0).exports.default.getToken()
```

Credit: [idk](https://pastebin.com/EG4YNbJD)


## Previews
<img src="https://roblox.is-terrible.com/n00neu.png"/>
<img src="https://roblox.is-terrible.com/sNwHE6.png"/>
<img src="https://roblox.is-terrible.com/TLq55b.png"/>
<img src="https://belle.is-inside.me/q7Jt0T96.png"/>

# Discord
Syz#0012 Send Nudes Pls